package main

import (
	"html/template"
	"log/slog"
	"net/http"
	"os"

	"github.com/a-h/templ"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/adaptor"
	"github.com/valyala/fasthttp"
)

var tmpl *template.Template

func main() {
	if err := loadConfig(); err != nil {
		slog.Error("Unable to load config", err)
		os.Exit(1)
	}

	app := fiber.New()
	app.Get("/+", getHandler)

	if err := app.Listen(cfg.ListenAddr); err != nil {
		slog.Error("Unable to listen to address", slog.String("address", cfg.ListenAddr), err)
		os.Exit(1)
	}
}

// getHandler is the (go get) endpoint handler.
func getHandler(c *fiber.Ctx) error {
	repName := c.Params("+")
	if repName == "" {
		slog.Error("Expected repository name: got nothing")

		return c.SendStatus(http.StatusBadRequest)
	}

	var repo RepoEntry
	var found bool

	for _, rep := range cfg.Repos {
		if rep.Name == repName {
			found = true
			repo = rep

			break
		}
	}

	if !found {
		slog.Warn("Unknown repository", slog.String("pkg_name", repName))

		return c.SendStatus(http.StatusNotFound)
	}

	// request is not bade by `go get` => redirect to the repo
	if c.Query("go-get") != "1" {
		c.Set(fasthttp.HeaderLocation, repo.VCSPath)

		return c.SendStatus(http.StatusTemporaryRedirect)
	}

	c.Set(fasthttp.HeaderCacheControl, "public, max-age=300")
	c.Set(fasthttp.HeaderContentType, "text/html")

	compHandler := templ.Handler(repoResponse(repo))

	return adaptor.HTTPHandler(compHandler)(c)
}
