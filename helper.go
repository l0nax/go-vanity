package main

import "strings"

func generateGoSourceContent(repo RepoEntry) string {
	var buf strings.Builder
	buf.Grow(256)

	// code.gyt.is/webimg https://source.gyt.is/gytisrepecka/webimg https://source.gyt.is/gytisrepecka/webimg/src/branch/master{/dir} https://source.gyt.is/gytisrepecka/webimg/src/branch/master{/dir}/{file}#L{line}

	buf.WriteString(repo.ImportPath)
	buf.WriteByte(' ')

	buf.WriteString(repo.VCSPath)
	if repo.VCSType == "git" {
		buf.WriteString(".git")
	}

	buf.WriteByte(' ')

	// TODO: Set default
	branch := repo.Branch

	buf.WriteString(repo.VCSPath)
	buf.WriteString("/-/tree/")
	buf.WriteString(branch)
	buf.WriteString("{/dir}")
	buf.WriteByte(' ')

	buf.WriteString(repo.VCSPath)
	buf.WriteString("/-/blob/")
	buf.WriteString(branch)
	buf.WriteString("{/dir}/{file}#L{line}")

	return buf.String()
}
