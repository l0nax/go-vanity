# Go Vanity

A very simple HTTP server which acts as "proxy" allowing to use a custom domain in golang modules. 

## Getting Started

See [deploy](./deploy/) for a deployment example.

The official OCI images can be found here: https://gitlab.com/l0nax/go-vanity/container_registry/6002068

## Verification

All the images and artifacts are signed by using [cosign's keyless signing](https://docs.sigstore.dev/cosign/openid_signing/)
The images can be verified with one of the following commands:
```bash
$ cosign verify registry.gitlab.com/l0nax/go-vanity:<tag> \
    --certificate-identity=emanuel@l0nax.org --certificate-oidc-issuer=https://github.com/login/oauth

## Example for v0.1.1
$ cosign verify registry.gitlab.com/l0nax/go-vanity:0.1.1 \
    --certificate-identity=emanuel@l0nax.org --certificate-oidc-issuer=https://github.com/login/oauth
```
