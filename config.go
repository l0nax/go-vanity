package main

import (
	"log/slog"
	"os"
	"strings"

	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/file"
	"github.com/knadh/koanf/v2"
	"github.com/pkg/errors"
)

// Config defines the application config structure.
type Config struct {
	// ListenAddr is the listen address.
	//
	// Changing this value requires a server restart.
	ListenAddr string `koanf:"listen_addr"`

	// Repos configures the known repositories.
	Repos []RepoEntry `koanf:"repos"`
}

// VCSHostingType is the hosting platform where the source is stored.
type VCSHostingType string

// The different hosting providers.
const (
	VCSUnknown VCSHostingType = ""
	VCSGitLab  VCSHostingType = "gitlab"
)

func (v VCSHostingType) IsKnown() bool {
	switch v {
	case VCSGitLab:
		return true
	}

	return false
}

// RepoEntry represents a single repository config entry.
type RepoEntry struct {
	// Name is the raw package name.
	Name string `koanf:"name"`

	// VCSPath is the path to the VCS providing the module source.
	VCSPath string `koanf:"vcs_path"`

	// ImportPath is the module path of the repository
	// which will be used in the "import" statement of Go files.
	ImportPath string `koanf:"import_path"`

	// VCSType is the type of the VCS system.
	// Supported type: "git".
	VCSType string `koanf:"vcs_type"`

	// ProviderType is the platform hosting the Git repository.
	ProviderType VCSHostingType `koanf:"provider_type"`

	// Branch is the branch the entry should point to for go-sources.
	// Defaults to "master" if not defined.
	Branch string `koanf:"branch"`
}

func (c *Config) mergeDefault() {
	if c.ListenAddr == "" {
		c.ListenAddr = ":3000"
	}
}

func generateMetaURLContent(repo RepoEntry) string {
	var buf strings.Builder
	buf.Grow(256)

	buf.WriteString("0; url=")
	buf.WriteString(repo.VCSPath)

	return buf.String()
}

func generateGoImportContent(repo RepoEntry) string {
	var buf strings.Builder
	buf.Grow(256)

	buf.WriteString(repo.ImportPath)
	buf.WriteByte(' ')

	buf.WriteString(repo.VCSType)
	buf.WriteByte(' ')

	buf.WriteString(repo.VCSPath)
	if repo.VCSType == "git" {
		buf.WriteString(".git")
	}

	return buf.String()
}

func (c *Config) validate() error {
	knownRepos := make(map[string]struct{}, len(c.Repos))
	for _, rep := range c.Repos {
		_, ok := knownRepos[rep.Name]
		if ok {
			return errors.Errorf("a repository with the name %q has already been defined", rep.Name)
		}

		knownRepos[rep.Name] = struct{}{}
	}

	return nil
}

// cfg is the global [Config].
var cfg *Config

func loadConfig() error {
	k := koanf.New(".")
	cfg = new(Config)

	fileProv := os.Getenv("VANITY_CONFIG")
	if fileProv == "" {
		fileProv = "/etc/go-vanity/config.yaml"
	}

	fp := file.Provider(fileProv)
	if err := k.Load(fp, yaml.Parser()); err != nil {
		return errors.Wrap(err, "unable to load config file")
	}

	fp.Watch(func(_ any, err error) {
		slog.Info("Config has been changed: reloading")

		if err != nil {
			slog.Error("An error occurred while watching the config file", err)
			os.Exit(1)
		}

		// force a config refresh
		k = koanf.New(".")
		if err := k.Load(fp, yaml.Parser()); err != nil {
			slog.Error("Unable to reload config", err)
			os.Exit(1)
		}

		if err := k.Unmarshal("", cfg); err != nil {
			slog.Error("Unable to parse config", err)
			os.Exit(1)
		}

		cfg.mergeDefault()

		if err := cfg.validate(); err != nil {
			slog.Error("Unable to validate config", err)
			os.Exit(1)
		}
	})

	if err := k.Unmarshal("", cfg); err != nil {
		return errors.Wrap(err, "unable to parse config")
	}

	cfg.mergeDefault()

	return cfg.validate()
}
